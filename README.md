![alt Sledgehammer Logo](public/images/logo/sledgehammer_logo_v1_153-100.png)

# Sledgehammer

This is a blogging tool built with the help of composer. Not really using a standard  php framework, instead using tools available.


## Features


## Dev Features


## Docs


## License
***BSD 3-Clause "New" or "Revised" License***

A permissive license similar to the BSD 2-Clause License, but with a 3rd clause that prohibits others from using the name of the project or its contributors to promote derived products without written consent.

See [LICENSE](LICENSE) for details.

**Permissions**
* Commercial use
* Modification
* Distribution
* Private use

**Limitations**
* No Liability
* No Warranty

**Conditions**
* License and copyright notice
  * -must be included with the software.
