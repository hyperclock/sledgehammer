<?php declare(strict_types = 1);

namespace App\Controllers;

use Http\Response;

class Homepage
{
    private $response;

    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    public function show()
    {
        $this->response->setContent('<h3>Hello World!</h3>');
    }
}
